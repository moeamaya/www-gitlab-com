---
layout: markdown_page
title: "Quality"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
- Chat channels; please use chat channels for questions that don't seem appropriate to use the issue tracker for.
  - [#qa](https://gitlab.slack.com/archives/qa): QA pipelines post into this
    channel, QA engineers should monitor this channel to act on alerts. "Acting
    on" may be remediating or just fixing noisy alerts.

## Quality goals and team(s)

### High level goals

* Stabilize & improve the release process
  * What should we be checking prior to each release, when & who is responsible
  * When and how should automated tests be run
  * Learn from common or frequent mistakes & regressions
* Improve [GitLab QA] test coverage, reliability and efficiency
  * Run-time, de-duplication
  * Which tests should run when
  * How can we put test result data in front of the right people & have them act on it
* Gain insight into development & test metrics
  * See: [https://gitlab.com/gitlab-org/triage/issues/1](https://gitlab.com/gitlab-org/triage/issues/1)
* Long-term improvements to the development process
  * Unit test coverage
  * CE/EE maintenance
  * Developers contributing to [GitLab QA] scenarios
  * Integrate feedback from Sales & Customer Support

### Teams in Quality

- **[Edge](edge)**: Improving the development process, and codebase quality/maintenance
- **QA / Test Automation**: Helping to improve the quality of the product with end-to-end automated testing

## Projects

* [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* [GitLab QA]
* Release Process improvements
  * First Deployable RC
      * Automated tests are run against Staging and Canary
      * An issue is created listing the QA tasks for this release candidate
          * The items from the Release Kickoff Doc are copied to this Issue and associated with the appropriate PM
          * The PM performs testing on staging and lists any issues found
      * After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed
      * Automated tests are run against Staging and Canary
  * Subsequent RCs
      * An issue is created listing the QA tasks for this release candidate
          * The changes that had been merged in since the previous release are captured in a QA task Issue, associated with the appropriate engineer
          * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing
  * Final Release
      * Run automated test suite against Staging and Canary (sanity check)
      * Since no changes should have been included between the last RC and the release-day build, no additional testing or review should be required.
* Recruiting
  * Information regarding the technical quizzes and assignments that are sent to candidates can be found [here](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires/blob/master/automation-engineer.md) (GITLAB ONLY)
  * The steps of the hiring process can be found [here](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/engineering.md)

## Other Related Pages

- [Engineering](/handbook/engineering)
- [Issue Triage](issue-triage)
- [Issue Triage Policies](/handbook/engineering/issue-triage)
- [Performance of GitLab](/handbook/engineering/performance)
- [Monitoring of GitLab.com](/handbook/infrastructure/monitoring)
- [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
